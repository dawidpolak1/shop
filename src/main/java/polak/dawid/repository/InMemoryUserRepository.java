package polak.dawid.repository;

import org.springframework.stereotype.Repository;
import polak.dawid.domain.Item;
import polak.dawid.domain.User;

import java.util.HashMap;
import java.util.Map;

@Repository
public class InMemoryUserRepository {

    private final Map<Integer, User> database = new HashMap<>();
    private Integer counter = 1;

    public User save(String name) {
        return setData(name);
    }

    private User setData(String name) {
        User user = new User(name);
        user.setUserId(counter);
        database.put(counter, user);
        counter++;

        return user;
    }

    public Map<Integer, User> getDatabase() {
        return database;
    }
}
