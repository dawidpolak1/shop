package polak.dawid.repository;

import org.springframework.stereotype.Repository;
import polak.dawid.domain.Item;
import polak.dawid.domain.User;

import java.util.HashMap;
import java.util.Map;

@Repository
public class InMemoryItemRepository {

    private final Map<Integer, Item> cart = new HashMap<>();
    private Integer counter = 1;

    public Item save(Item item) {
        if (item.getItemId() != null) {
            cart.put(item.getItemId(), item);
        } else {
            item.setItemId(counter);
            cart.put(counter, item);
            counter++;
        }

        return item;
    }
}
