package polak.dawid.domain;

public class Item {

    private Integer itemId;
    private final String name;
    private final int quantity;
    private final double price;

    public Item(Integer itemId, String name, int quantity, double price) {
        this.itemId = itemId;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    public Item(String name, int quantity, double price) {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    public Integer getItemId() {
        return itemId;
    }

    public String getName() {
        return name;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }
}
