package polak.dawid.domain;

import java.util.ArrayList;
import java.util.List;

public class User {

    private Integer userId;
    private String name;
    private List<Item> items = new ArrayList<>();

    public User(Integer userId, String name) {
        this.userId = userId;
        this.name = name;
    }

    public User(String name) {
        this.name = name;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
