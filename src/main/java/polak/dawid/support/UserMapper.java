package polak.dawid.support;

import org.springframework.stereotype.Component;
import polak.dawid.api.request.UserRequest;
import polak.dawid.api.response.UserResponse;
import polak.dawid.domain.User;

@Component
public class UserMapper {

    public String toUser(UserRequest userRequest) {
        return userRequest.getName();
    }

    public UserResponse toUserResponse(User user) {
        return new UserResponse(user.getUserId(), user.getName());
    }
}
