package polak.dawid.support;

import org.springframework.stereotype.Component;
import polak.dawid.api.request.ItemRequest;
import polak.dawid.api.response.ItemResponse;
import polak.dawid.domain.Item;

@Component
public class ItemMapper {

    public Item toItem(ItemRequest itemRequest) {
        return new Item(itemRequest.getName(), itemRequest.getQuantity(), itemRequest.getPrice());
    }

    public ItemResponse toItemResponse(Item item) {
        return new ItemResponse(item.getItemId(), item.getName(), item.getQuantity(), item.getPrice());
    }
}
