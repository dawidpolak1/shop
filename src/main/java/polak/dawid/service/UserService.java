package polak.dawid.service;

import org.springframework.stereotype.Service;
import polak.dawid.api.request.UserRequest;
import polak.dawid.api.response.UserResponse;
import polak.dawid.domain.User;
import polak.dawid.repository.InMemoryUserRepository;
import polak.dawid.support.UserMapper;

import java.util.Map;

@Service
public class UserService {

    private final InMemoryUserRepository userRepository;
    private final UserMapper userMapper;

    public UserService(InMemoryUserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    public UserResponse create(UserRequest userRequest) {
        String name = userRequest.getName();
        if (userExists(name)) {
            throw new IllegalArgumentException("User " + name + " already exists in database");
        }

        User user = userRepository.save(userMapper.toUser(userRequest));
        return userMapper.toUserResponse(user);
    }

    private boolean userExists(String name) {
        Map<Integer, User> database = userRepository.getDatabase();
        return database.containsValue(name);
    }
}
