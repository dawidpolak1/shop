package polak.dawid.service;

import org.springframework.stereotype.Service;
import polak.dawid.api.request.ItemRequest;
import polak.dawid.api.response.ItemResponse;
import polak.dawid.domain.Item;
import polak.dawid.repository.InMemoryItemRepository;
import polak.dawid.support.ItemMapper;

@Service
public class ItemService {

    private final InMemoryItemRepository itemRepository;
    private final ItemMapper itemMapper;

    public ItemService(InMemoryItemRepository itemRepository, ItemMapper itemMapper) {
        this.itemRepository = itemRepository;
        this.itemMapper = itemMapper;
    }

    public ItemResponse create(ItemRequest itemRequest) {
        Item item = itemRepository.save(itemMapper.toItem(itemRequest));
        return itemMapper.toItemResponse(item);
    }
}
