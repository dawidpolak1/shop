package polak.dawid.api.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import polak.dawid.api.response.ItemResponse;
import polak.dawid.api.response.UserResponse;
import polak.dawid.service.UserService;

public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity<ItemResponse> create() {
        return null;
    }
}
