package polak.dawid.api.response;

import polak.dawid.domain.Item;

import java.util.ArrayList;
import java.util.List;

public class UserResponse {

    private final Integer userId;
    private final String name;
    private final List<Item> items = new ArrayList<>();

    public UserResponse(Integer userId, String name) {
        this.userId = userId;
        this.name = name;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public List<Item> getItems() {
        return items;
    }
}
