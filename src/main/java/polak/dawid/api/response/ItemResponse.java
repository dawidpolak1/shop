package polak.dawid.api.response;

public class ItemResponse {

    private final Integer itemId;
    private final String name;
    private final double price;
    private final int quantity;

    public ItemResponse(Integer itemId, String name, int quantity, double price) {
        this.itemId = itemId;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    public Integer getItemId() {
        return itemId;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }
}
