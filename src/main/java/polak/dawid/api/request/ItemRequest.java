package polak.dawid.api.request;

import com.fasterxml.jackson.annotation.JsonCreator;

public class ItemRequest {

    private final String name;
    private int quantity;
    private double price;

    @JsonCreator
    public ItemRequest(String name) {
        this.name = name;
    }

    @JsonCreator
    public ItemRequest(String name, int quantity, double price) {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }
}
