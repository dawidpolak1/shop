package polak.dawid.api.request;

import com.fasterxml.jackson.annotation.JsonCreator;

public class UserRequest {

    private final String name;

    @JsonCreator
    public UserRequest(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
